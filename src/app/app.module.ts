import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatIconModule, MatRippleModule, MatPaginatorIntl } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { AppComponent } from './app.component';
import { getPtBrPaginatorIntl } from './ptbr-paginator-intl';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { RegisterComponent } from './movie/register/register.component';
import { RegisterModule } from './movie/register/register.module';
import { ListComponent } from './movie/list/list.component';
import { ListModule } from './movie/list/list.module';
import { HomeModule } from './movie/home/home.module';
import { GridModule } from './movie/grid/grid.module';
import { HomeComponent } from './movie/home/home.component';
import { GridComponent } from './movie/grid/grid.component';

const appRoutes: Routes = [
    {path: 'register', component: RegisterComponent},
    {path: 'grid', component: GridComponent},
    {path: 'list', component: ListComponent},
    {path: 'home', component: HomeComponent},
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: '**', redirectTo: 'home'}
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports     : [
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes, {useHash: false}),

        TranslateModule.forRoot(),

        ToastrModule.forRoot({ 
            timeOut: 3000,
            positionClass: 'toast-top-center',
            preventDuplicates: true
        }),

        // Material moment date module
        MatMomentDateModule,

        // // Material
        MatButtonModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule, 

        HomeModule,
        RegisterModule,
        ListModule,
        GridModule,
    ],
    exports : [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule 
    ],
    providers: [ToastrService,
        { provide: MatPaginatorIntl, useValue: getPtBrPaginatorIntl() }
      ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
