import { Injectable } from '@angular/core';

import { HttpParams} from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class GlobalsService {
  
  private toastr: ToastrService;
  public spinner = new NgxSpinnerService();

  //privates
  private params: HttpParams;
  window: any = window;

  formataNumero = function (vString) {
    vString += "";
    //console.log(parseFloat("40000000.5"));
    if (vString) {
      vString = vString.replace(/[.]/g, "");
      //console.log(vString);

      vString = vString.replace(/[,]/g, ".");
      //console.log(vString);

      vString = parseFloat(vString);
      //console.log(vString);
    }
    return vString;
  };

  formataNumeroReal = function (string) {

    if (string) {
      string = string.toString().replace(/[^0-9,.]/g, "");
      var xValor = string.replace(/[.,]/g, "");

      var xReal = xValor.substr(0, xValor.length - 2);
      var xCentavos = xValor.substr(-2, 2);

      if (xCentavos.length === 1) {
        xCentavos = "0" + xCentavos;
      }

      string = this.window.accounting.formatNumber(this.formataNumero(xReal + "," + xCentavos), this.parametrosNumericosReal);

    } else {
      string = "0,00";
    }

    return string;
  };


}
