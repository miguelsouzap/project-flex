import { Component, Inject, OnDestroy, OnInit, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Platform } from '@angular/cdk/platform'; 
export let browserRefresh = false;

@Component({
    selector: "app-root",
    template: "<router-outlet></router-outlet>"
  })

export class AppComponent
{

    @HostListener('window:unload', ['$event'])
    unloadHandler(event) {
    }
    /**
     * Constructor
     *
     * @param {DOCUMENT} document
     * @param {Platform} _platform
     */
    constructor(
        @Inject(DOCUMENT) private document: any,
        private _platform: Platform,
    )
    {
        // Add is-mobile class to the body if the platform is mobile
        if ( this._platform.ANDROID || this._platform.IOS )
        {
            this.document.body.classList.add('is-mobile');
        }

    }

}
