import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import createAutoCorrectedDatePipe from 'text-mask-addons/dist/createAutoCorrectedDatePipe'
import { MatPaginator, DateAdapter } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { GlobalsService } from '../../globals.service';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent {
  
  httpOption: {} = {};
  globals: GlobalsService = new GlobalsService();
  movieService: MovieService;
  
  movie: any;
  movieForm: FormGroup;
  typeMovieForm: FormControl;
  typeProductForm: FormControl;
  coverPhotoForm: FormControl;
  mainDirectorsForm: FormControl;
  mainActorsForm: FormControl;
  genreForm: FormControl;
  providerForm: FormControl;
  languageTypeForm: FormControl;
  movieNumberForm: FormControl;
  movieTitleForm: FormControl;
  valueForm: FormControl;
  registerDateForm: FormControl;

  submitted = false;
  
  createFormControls() {
    this.typeMovieForm = new FormControl('', Validators.required);
    this.typeProductForm = new FormControl('', Validators.required);
    this.coverPhotoForm = new FormControl('', Validators.required);
    this.mainActorsForm = new FormControl('', Validators.required);
    this.mainDirectorsForm = new FormControl('', Validators.required);
    this.genreForm = new FormControl('', Validators.required);
    this.providerForm = new FormControl('', Validators.required);
    this.languageTypeForm = new FormControl('', Validators.required);
    this.movieNumberForm = new FormControl('', Validators.required);
    this.movieTitleForm = new FormControl('', Validators.required);
    this.valueForm = new FormControl('', Validators.required);
    this.registerDateForm = new FormControl('', Validators.required);
  }

  createForm() { 
    this.movieForm = new FormGroup({
      typeMovieForm: this.typeMovieForm,
      typeProductForm: this.typeProductForm,
      coverPhotoForm: this.coverPhotoForm,
      mainActorsForm: this.mainActorsForm,
      mainDirectorsForm: this.mainDirectorsForm,
      genreForm: this.genreForm,
      providerForm: this.providerForm,
      languageTypeForm: this.languageTypeForm,
      movieNumberForm: this.movieNumberForm,
      movieTitleForm: this.movieTitleForm,
      valueForm: this.valueForm,
      registerDateForm: this.registerDateForm
    });
  }

  public mask = {
    guide: true,
    showMask: true,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
    pipe: createAutoCorrectedDatePipe('dd.mm.yyyy')
  };

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private http: HttpClient,
              private route: ActivatedRoute,
              private toastr: ToastrService,
              private router: Router,
              private adapter: DateAdapter<any>
            ) {
              
      this.route.params
      .subscribe(params => {
        if( params.name ){
          this.movie = params
        } else {
          this.newMovie()
        }
      });        

      this.movieService = new MovieService(this.http);      
      this.createFormControls();
      this.createForm();
      
     
  }

  back(){
    this.router.navigate(['../list'], {skipLocationChange: true});
  }

  goHome(){
    this.router.navigate(['../home'], {skipLocationChange: true});
  }

  newMovie(){
    this.movie = {
      typeMovie: "",
      typeProduct: "",
      coverPhoto: "",
      mainActors: "",
      mainDirectors: "",
      genre: "",
      provider: "",
      languageType: "",
      movieNumber: "",
      movieTitle: "",
      value: "",
      registerDate: ""
    };
  }

  updateDate() {
    this.adapter.setLocale('pt');
  }

  registerMovie(movie){
    this.toastr.success("Filme salvo com sucesso");
    this.router.navigate(['/home'], {skipLocationChange: true});
  }

  onSelectFile(event: Event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.movie.coverPhoto = event.target.result;
      }
    }
}

}
