import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  MAT_DATE_LOCALE, 
  DateAdapter, 
  MAT_DATE_FORMATS,
} from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { FormsModule, ReactiveFormsModule, NgForm } from '@angular/forms';
import { RegisterComponent } from './register.component';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatIconModule, MatRippleModule, MatPaginatorIntl } from '@angular/material';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { TextMaskModule } from 'angular2-text-mask';

const routes = [
  {
      path: 'movie/register',
      component: RegisterComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CurrencyMaskModule,
    TextMaskModule,
    ReactiveFormsModule,
    FormsModule,  
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule, 
  ],
  declarations: [
    RegisterComponent,
  ],
  exports: [],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class RegisterModule { }
