import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  MatCardModule,
  MatGridListModule
} from '@angular/material';
import { GridComponent } from './grid.component';

const routes = [
  {
      path     : 'grid',
      component: GridComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    MatCardModule,
    MatGridListModule
  ],
  declarations: [
    GridComponent,
  ],
  exports: [],
  providers: [],
})
export class GridModule { }
