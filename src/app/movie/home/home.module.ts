import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  MatCardModule,
  MatGridListModule
} from '@angular/material';
import { HomeComponent } from './home.component';

const routes = [
  {
      path     : 'home',
      component: HomeComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    MatCardModule,
    MatGridListModule
  ],
  declarations: [
    HomeComponent,
  ],
  exports: [],
  providers: [],
})
export class HomeModule { }
