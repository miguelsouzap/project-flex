import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, DateAdapter } from '@angular/material';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private http: HttpClient,
              private toastr: ToastrService,
              private router: Router,
              private adapter: DateAdapter<any>
            ) {

  }

  goRegister(){
    this.router.navigate(['../register']);
  }

  goList(){
    this.router.navigate(['../list']);
  }

}
