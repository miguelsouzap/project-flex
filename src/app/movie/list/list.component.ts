import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';

import { MovieService } from '../movie.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import 'hammerjs'; 

@Component({
  selector   : 'list',
    templateUrl: './list.component.html',
    styleUrls  : ['./list.component.css'],
})

export class ListComponent {

  httpOption: any;
  isMobile: any;

  public movieList: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public displayedColumns = ['name', 'year', 'genre', 'description', 'url', 'actions'];
  movieService: MovieService;

    constructor(private http: HttpClient,
              public toastr: ToastrService,
              public router: Router) { 
    this.movieService = new MovieService(this.http);      
    this.listMovies()  
  }

  goRegister(){
    this.router.navigate(['../register'], {skipLocationChange: true});
  }

  listMovies(){
    this.movieService.listMovies().subscribe((data: any) => {
      this.movieList = new MatTableDataSource(data);   
    }, error => { 
    });
  }

  detailsMovie(movie){
    this.router.navigate(['../register', movie], {skipLocationChange: true});
  }

}
