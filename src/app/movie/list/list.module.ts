import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatGridListModule, 
    MatCardModule,
    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule, 
    MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatIconModule, MatDatepickerModule, MatDividerModule } from '@angular/material';

import { ListComponent } from './list.component';
import { FormsModule } from '@angular/forms';

const routes = [
    {
        path: 'movie/list',
        component: ListComponent
    }
];

@NgModule({
    declarations: [
        ListComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        // FormsModule,  
        MatGridListModule,
        MatDividerModule,
        MatSelectModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatSortModule,
        MatTableModule,
        MatIconModule,
        MatPaginatorModule,
        MatTooltipModule    
    ],
    providers: [],
    entryComponents: [] 
})
export class ListModule
{
}

