import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MovieService {
  constructor(private http: HttpClient) {
  }

  public listMovies(){
    return this.http.get('/assets/movies.json')
  }

  public saveMovie(movie){
    return this.http.post('/assets/movies.json', JSON.stringify(movie))
  }
}
